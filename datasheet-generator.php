<?php
/**
 * Plugin Name: DataSheet Generator
 * Description: This plugin generates dataSheet of a property
 * Version: 1.0.0
 * Author: Waqas Raza
 * Text Domain: datasheet-plugin
 */

defined('ABSPATH') or die('Hay, What are you doing here? You silly human');

function dirname_r($path, $count=1){
    if ($count > 1){
        return dirname(dirname_r($path, --$count));
    }else{
        return dirname($path);
    }
}
if( file_exists( dirname( __FILE__ ). '/vendor/autoload.php' ) )
{
    require_once dirname( __FILE__ ). '/vendor/autoload.php';
}

function activate_datasheet_plugin()
{
    \Inc\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'activate_datasheet_plugin' );

function deactivate_datasheet_plugin()
{
    \Inc\Base\Deactivate::deactivate();
}

register_deactivation_hook( __FILE__, 'deactivate_datasheet_plugin' );

if( class_exists( 'Inc\\Init' ) )
{
    Inc\Init::register_services();
}