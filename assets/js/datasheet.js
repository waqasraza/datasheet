/**
 * Created by pak on 8/31/2019.
 */
function createPdf(e)
{
    jQuery(e).addClass('hide');
    jQuery(e).prev('img').removeClass('hide');

    var estate_id =  parseInt(jQuery(e).data('id'));

    if( estate_id > 0)
    {
        $.ajax({
            url: datasheet.ajax_url,
            type:'post',
            dataType: 'json',
            data:{'estate_id': estate_id, 'action':'createPdf' }
        }).done(function( resp ){
            printDiv(resp);
        })
    }
}
function printDiv(html) {
    var printContents = html;
    var originalContents = document.body.innerHTML;

    jQuery('#datasheet-pdf').html(printContents);
    jQuery('#datasheet-pdf').removeClass('hide');
    jQuery('#wpwrap').addClass('hide');

}
function datasheetPrint(){
    window.print();
}
function datasheetCancel(){
    jQuery('#datasheet-pdf').addClass('hide');
    jQuery('#wpwrap').removeClass('hide');
    jQuery('body').removeClass('sticky-menu');

    jQuery('.btn-datasheet').removeClass('hide');
    jQuery('.ajax-preloader').addClass('hide');
}

jQuery(document).ready(function(){
    jQuery('.btn-datasheet').removeClass('hide');
    jQuery('.ajax-preloader').addClass('hide');
    jQuery('body').append('<div id="datasheet-pdf" class="hide"></div>');
});


/*Methods for Adding Keyword into listings*/

function startAddingKeywordIntoLising(){
    var listings = getAllLisings();
}

function getAllLisings(){
    $.ajax({
        url: datasheet.ajax_url,
        type:'post',
        dataType: 'json',
        data:{'action':'addKeyWordIntoListings' }
    }).done(function( resp ){
        console.log(resp);
    })
}

function changeRegionSelectBox(e) {
    var $this       = e;
    var options     =  $this.options;
    var region_slug = options[$this.selectedIndex].value;
    var city_select = document.getElementById('list-city');

    city_select.setAttribute('disabled', 'disabled');
    $.ajax({
        url: datasheet.ajax_url,
        type:'post',
        dataType: 'json',
        data:{'slug': region_slug, 'action':'changeRegionSelectBox' }
    }).done(function( resp ){
        city_select.innerHTML = resp.html;
        city_select.removeAttribute('disabled');
    });
}