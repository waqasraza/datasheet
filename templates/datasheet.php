<?php
global $myhome_redux;
$map_key = $myhome_redux['mh-google-api-key'];
$myhome_estate = \MyHomeCore\Estates\Estate::get_instance( $estate_id );
$term = wp_get_post_terms( $myhome_estate->get_ID(), 'city' );
$city_image_url = false;
if( count( $term ) AND  !is_wp_error( $term[0] ) ){
    $myhome_term = new \MyHomeCore\Terms\Term( $term[0] );
    if( $myhome_term->has_image() ){
        $city_image_url = wp_get_attachment_image_url( $myhome_term->get_image_id() );
    }
}

if( wp_get_current_user() ){
    $myhome_agent = MyHomeCore\Users\User::get_instance( wp_get_current_user() );
}else{
    $myhome_agent = $myhome_estate->get_user();
}

$offer_types = $myhome_estate->get_offer_type();
$gallery_images = $myhome_estate->get_gallery_data(0, 'xs');
$types = array();
if( count( $offer_types ) )
{
    foreach( $offer_types as $key => $offer_type )
    {
        $types[$key]['name'] = $offer_type->get_name();
        $types[$key]['slug'] = $offer_type->get_slug();
    }
}

$offer_type_names = array();

if( count( $types ) ) {
    foreach( $types as $offer_type ) {
        $offer_type_names[] = $offer_type['name'];
    }
}
$selling_point_text_field_1 = '';
$selling_point_text_field_2 = '';
$property_type = '';
//$reference_no = '';
$address = '';
$offer_type = '';
$housestatus = '';
$nearest_railway_station = '';
$size_in_meter = '';
$property_age = '';
$jp_price = '';
$hkd_price = '';
$actual_roi = '';
$facial_roi = '';
$management_fee = '';
$maintenance_fund = '';
$rent = '';
$leasing_commission = '';
$property_new_old = '';
$land_rights = '';
$balcony_area = '';
$bedroom = '';
$floors = '';
$floors_total = '';
$households_total = '';
$manage_method = '';
$parking = '';
$building_structure = '';

# Finance information
$hkd_price =$myhome_estate->getHKDPrice();
$jp_price = $myhome_estate->getJPYPriceOnly();
$actual_roi = $myhome_estate->getActualRoi();
$facial_roi = $myhome_estate->getFacialRoi();

foreach ( $myhome_estate->get_attributes() as $myhome_attr ) {

    if (!$myhome_attr->has_values() || $myhome_attr->new_box()) continue;

    switch ( $myhome_attr->get_name() ) {
        # Basic information
        case "所在地": //Address
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $address = $myhome_attr_value->get_name();
            }
            break;
        case "物件類別": //Property Type
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $property_type = $myhome_attr_value->get_name();
            }
            break;
        case "Property Type New": //Property Type New
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $property_type_new = $myhome_attr_value->get_name();
            }
            break;
        /*case "參考編號": //Reference No
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $reference_no = $myhome_attr_value->get_name();
            }
            break;*/
        case "出售/出租": //Offer type
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $offer_type = $myhome_attr_value->get_name();
            }
            break;
        case "現況": //Housestatus
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $housestatus = $myhome_attr_value->get_name();
            }
            break;
        case "交通": //Nearest Railway Station
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $nearest_railway_station = $myhome_attr_value->get_name();
            }
            break;
        case "面積": //Size In Meter
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $size_in_meter = $myhome_attr_value->get_name();
            }
            break;
        /*case "建築年月": //Property Age
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $property_age = $myhome_attr_value->get_name();
            }
            break;*/

        # Finance information
        case "單位管理費": //Management Fee
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $management_fee = $myhome_attr_value->get_name();
            }
            break;
        case "修繕積立金": //Maintenance Fund
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $maintenance_fund = $myhome_attr_value->get_name();
            }
            break;
        case "現租金/預想租金": //Rent
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $rent = $myhome_attr_value->get_name();
            }
            break;
        case "租務委托 (含消費稅)": //Leasing Commission
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $leasing_commission = $myhome_attr_value->get_name();
            }
            break;

        # Other information

        case "土地權利": //Land Rights
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ){
                $land_rights = $myhome_attr_value->get_name();
            }
            break;
        case "陽台面積": //Balcony Area
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ){
                $balcony_area = $myhome_attr_value->get_name();
            }
            break;
        case "戶型": //Bedroom
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ){
                $bedroom = $myhome_attr_value->get_name();
            }
            break;
        case "所在階": //Floors
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ){
                $floors = $myhome_attr_value->get_name();
            }
            break;
        case "總階建": //Floors Total
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $floors_total = $myhome_attr_value->get_name();
            }
            break;
        case "總戶數": //Households Total
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $households_total = $myhome_attr_value->get_name();
            }
            break;
        case "管理形態": //Manage Method
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $manage_method = $myhome_attr_value->get_name();
            }
            break;
        case "駐車場": //Parking
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $parking = $myhome_attr_value->get_name();
            }
            break;
        case "建物構造": // Building structure
            foreach ( $myhome_attr->get_values() as $myhome_attr_key => $myhome_attr_value ) {
                $building_structure = $myhome_attr_value->get_name();
            }
            break;

    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title> Trusty Group </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style rel="stylesheet" href="<?php echo $this->plugin_url ?>assets/css/datasheet.css" type="text/css" media="all"></style>
    <?php
    $base_color = '#7f440a';
    $gradient = '#f29a40';
    ?>
    <style>
        .navbar-default {
            background-image: linear-gradient(to right, <?php echo $base_color ?> , <?php echo $gradient; ?>);
        }
        .map-content {
            background-image: linear-gradient(to right, <?php echo $base_color;  ?> ,<?php echo $gradient; ?>);
        }
        #features .name {
            background-color: <?php echo $base_color ?>;
        }
        #cities {
            <?php if( !empty( My_Home_Theme()->settings->get( 'data-info-japan-cities-image' ) ) ): ?>
                background-image: url(<?php echo My_Home_Theme()->settings->get( 'data-info-japan-cities-image' )['url']; ?>);
            <?php else: ?>
                background-color: white;
            <?php endif; ?>
        }
    </style>
</head>
<body>
<button id="btn-datasheet-print" class="no-print" onclick="datasheetPrint()">Print</button>
<button id="btn-datasheet-cancel" class="no-print" onclick="datasheetCancel()">Cancel</button>
<div id="content" class="container">
    <header class="navbar-default">
        <table class="table">
            <tr>
                <td width="2%%">
                    <table class="w100">
                        <tr>
                            <td width="50%">
                                <?php if( $city_image_url ):?>
                                    <img width="75" src="<?php echo $city_image_url; ?>">
                                <?php else: ?>
                                    <img width="75" src="<?php echo $this->plugin_url; ?>assets/img/building_logo.png">

                                <?php  endif; ?>
                            </td>
                            <td width="30%">
                                <div class="box" style="width: 32px;">
                                    <div class="box area-name">
                                        <?php if( count( $myhome_estate->getCity() ) ): ?>
                                            <?php echo $myhome_estate->getCity()[0]['name']; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="10%">
                    <div class="box" style="width: 90%;">
                        <span class="name" style="font-weight: bold;font-size: 23px;"> 面積</span>
                        <span class="value"><?php echo $myhome_estate->getSizeInMeter(); ?></span>
                    </div>
                </td>
                <td width="10%">
                    <?php if( $myhome_estate->getFacialRoi() > '0%' ): ?>

                    <div class="box" style="width: 90%;">
                        <span class="name" style="font-weight: bold;font-size: 23px;">表面回報</span>
                        <span class="value">
                                <?php echo $myhome_estate->getFacialRoi(); ?>
                        </span>
                    </div>
                    <?php endif; ?>

                </td>
                <td width="10%">
                    <?php if( $actual_roi > '0%' ): ?>

                    <div class="box" style="width: 90%;">

                        <span class="name" style="font-weight: bold;font-size: 23px;">實際回報</span>
                        <span class="value">
                                <?php echo $actual_roi; ?>
                        </span>
                    </div>
                    <?php endif ?>

                </td>
                <td width="10%">
                    <div class="box price">
                        <?php
                        $hkd_price = str_replace( 'HKD', '', $hkd_price );
                        $offerType =  count( $offer_types ) ? implode( '/', $offer_types ) : '';
                        ?>

                        <?php echo "<span style='font-size: 23px;font-weight: bold;color: white;'>$offerType <strong style='font-size: 23px;'>日元</strong></span>".number_format( $jp_price );  ?><strong style="font-size: 23px;">萬円</strong>
                        <?php if( ! empty( $hkd_price ) ): ?>
                            <div class="clearfix"></div>
                            <span style="font-size: 23px;font-weight: bold;color: white;"><?php echo $hkd_price ?></span>
                            <div class="clearfix"></div>
                        <?php endif; ?>
                    </div>
                </td>
            </tr>
        </table>
    </header>
    <div class="row mt5 mb5">

<!--        <div class="col col-left" style="height: 465px; overflow: hidden;">-->
            <div class="col col-left" style="overflow: hidden; height: 455px">
            <div id="features">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $myhome_estate->get_name(); ?></div>
                    <div class="panel-body">
                        <?php ?>
                        <!-- Selling Point -->
                        <?php if( get_field( 'selling_point_text_field_1', $myhome_estate->get_ID() ) OR get_field( 'selling_point_text_field_2', $myhome_estate->get_ID() ) ): ?>
                        <div class="row">
                            <div class="attribute-heading w100">物件優點</div>

                            <?php if( get_field( 'selling_point_text_field_1', $myhome_estate->get_ID() ) AND !get_field( 'selling_point_text_field_2', $myhome_estate->get_ID() ) ): ?>
                                <div class="row">
                                    <strong class="selling-points">
                                        <?php the_field( 'selling_point_text_field_1', $myhome_estate->get_ID() ) ?>
                                    </strong>
                                </div>
                            <?php endif; ?>

                            <?php if( !get_field( 'selling_point_text_field_1', $myhome_estate->get_ID() ) AND get_field( 'selling_point_text_field_2', $myhome_estate->get_ID() ) ): ?>
                                <div class="row">
                                    <strong class="selling-points">
                                        <?php the_field( 'selling_point_text_field_2', $myhome_estate->get_ID() ) ?>
                                    </strong>
                                </div>
                            <?php endif; ?>


                            <?php if( get_field( 'selling_point_text_field_1',$myhome_estate->get_ID() ) AND get_field( 'selling_point_text_field_2', $myhome_estate->get_ID() ) ): ?>
                            <div class="column">
                                <strong class="selling-points">
                                    <?php the_field( 'selling_point_text_field_1', $myhome_estate->get_ID() ) ?>
                                </strong>
                            </div>
                            <div class="column">
                                <strong class="selling-points">
                                    <?php the_field( 'selling_point_text_field_2', $myhome_estate->get_ID() ) ?>
                                </strong>
                            </div>
                            <?php endif; ?>
                        </div>
                        <?php endif ?>
                        <!-- Basic Information -->

                        <div class="row">
                            <div class="attribute-heading w100">基本資料</div>
                        </div>
                        <div class="row">
                            <?php if( ! empty( $address ) ): ?>
                            <div class="column w100">
                                <strong>所在地:</strong>
                                <span><?php echo $address; ?></span>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="row">
                            <?php if( ! empty( $property_type ) ): ?>
                                <div class="column">
                                    <strong>物件類別:</strong>
                                    <span><?php echo $property_type_new .' '. $property_type; ?></span>
                                </div>
                            <?php endif; ?>
                            <?php if( ! empty( $offer_type ) ): ?>
                            <div class="column">
                                <strong>出售/出租:</strong>
                                <span><?php echo $offer_type; ?></span>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="row">
                            <?php if( ! empty( $housestatus ) ): ?>
                            <div class="column">
                                <strong>現況:</strong>
                                <span><?php echo $housestatus; ?></span>
                            </div>
                            <?php endif; ?>
                            <?php if( ! empty( $size_in_meter ) ): ?>
                            <div class="column">
                                <strong>面積:</strong>
                                <span><?php echo $size_in_meter; ?></span>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="row">
                            <?php if( ! empty( $nearest_railway_station ) ): ?>
                            <div class="column w100">
                                <strong>交通:</strong>
                                <span><?php echo $nearest_railway_station; ?></span>
                            </div>
                            <?php endif; ?>
                        </div>

                        <!-- Finance Information -->

                        <div class="row">
                            <div class="attribute-heading w100">財務數據</div>
                        </div>
                        <div class="row">
                            <?php if( ! empty( $jp_price ) ): ?>
                            <div class="column w100">
                                <strong>售價:</strong>
                                <span><?php echo number_format( $jp_price ) .'円 ('.  $hkd_price .')'; ?></span>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="row">
                            <?php if( $actual_roi > '0%' ): ?>
                            <div class="column">
                                <strong>實際回報率:</strong>
                                <span><?php echo $actual_roi; ?></span>
                            </div>
                            <?php endif; ?>

                            <?php if(  $facial_roi > '0%' ): ?>
                            <div class="column">
                                <strong>表面回報率:</strong>
                                <span><?php echo  $facial_roi ?></span>
                            </div>
                            <?php endif; ?>
                        </div>

                        <div class="row">
                            <?php if( ! empty( $management_fee ) ): ?>
                            <div class="column">
                                <strong>單位管理費:</strong>
                                <span><?php echo number_format( $management_fee ); ?>円</span>
                            </div>
                            <?php endif; ?>

                            <?php if( ! empty( $maintenance_fund ) ): ?>
                            <div class="column">
                                <strong>修繕積立金:</strong>
                                <span><?php echo number_format( $maintenance_fund );  ?>円</span>
                            </div>
                            <?php endif; ?>
                        </div>

                        <div class="row">
                            <?php if( ! empty( $rent ) ): ?>
                                <div class="column w100">
                                    <strong>現租金/預想租金:</strong>
                                    <span><?php echo number_format( $rent ); ?>円</span>
                                </div>
                            <?php endif; ?>
                        </div>

                        <div class="row">
                            <?php if( ! empty( $leasing_commission ) ): ?>
                            <div class="column w100">
                                <strong> 租務委托 (含消費稅):</strong>
                                <span><?php echo number_format( $leasing_commission );  ?>円</span>
                            </div>
                            <?php endif; ?>
                        </div>

                        <!-- Other Information -->

                        <div class="row">
                            <div class="attribute-heading w100">其他資料</div>
                        </div>
                        <div class="row">
                            <?php if( ! empty( $land_rights ) ): ?>
                            <div class="column">
                                <strong>土地權利:</strong>
                                <span><?php echo $land_rights;  ?></span>
                            </div>
                            <?php endif; ?>
                            <?php if( ! empty( $balcony_area ) ): ?>
                            <div class="column">
                                <strong>陽台面積:</strong>
                                <span><?php echo $balcony_area; ?>平方米</span>
                            </div>
                            <?php endif; ?>
                        </div>

                        <div class="row">
                            <?php if( ! empty( $bedroom ) ): ?>
                            <div class="column">
                                <strong>戶型:</strong>
                                <span><?php echo $bedroom;  ?></span>
                            </div>
                            <?php endif; ?>
                            <?php if( ! empty( $floors ) ): ?>
                            <div class="column">
                                <strong>所在階:</strong>
                                <span><?php echo $floors; ?></span>
                            </div>
                            <?php endif; ?>
                        </div>

                        <div class="row">
                            <?php if( ! empty( $floors_total ) ): ?>
                            <div class="column">
                                <strong>總階建:</strong>
                                <span><?php echo $floors_total;  ?></span>
                            </div>
                            <?php endif; ?>
                            <?php if( ! empty( $households_total ) ): ?>
                            <div class="column">
                                <strong>總戶數:</strong>
                                <span><?php echo $households_total; ?></span>
                            </div>
                            <?php endif; ?>
                        </div>

                        <div class="row">
                            <?php if( ! empty( $manage_method ) ): ?>
                            <div class="column">
                                <strong>管理形態:</strong>
                                <span><?php echo $manage_method;  ?></span>
                            </div>
                            <?php endif; ?>
                            <?php if( ! empty( $parking ) ): ?>
                            <div class="column">
                                <strong>駐車場:</strong>
                                <span><?php echo $parking; ?></span>
                            </div>
                            <?php endif; ?>
                        </div>

                        <div class="row">
                            <?php if( ! empty( $building_structure ) ): ?>
                            <div class="column">
                                <strong>建物構造:</strong>
                                <span><?php echo $building_structure;  ?></span>
                            </div>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="col col-right">
            <div class="col col-left" style="width: 40.90%;border: none;padding: 0;">

                <div style="height: 218px;border: 1px solid <?php echo $base_color ;?>;">
                    <?php if( count( $myhome_estate->get_plans() ) ){ ?>

                            <div>
                            <?php //echo  esc_html( $myhome_estate->get_plans()[0]->get_label() );?>
                            </div>
                            <?php if( $myhome_estate->get_plans()[0]->get_image() ): ?>
                                <div style="background-image: url(<?php echo $myhome_estate->get_plans()[0]->get_image() ?>);
                                    height: 99.9%;
                                    width: 99.9%;
                                    background-position: center;
                                    background-size: contain;
                                    background-repeat: no-repeat;
                                    background-color: white;" >
                                    </div>
                            <?php endif; ?>
                    <?php }else{ ?>
<!--                        <div style="text-align: center; margin: 0 auto;">No Plan</div>-->
                    <?php } ?>

                </div>

                <div style="border: 1px solid <?php echo $base_color ;?>;padding: 5px;margin-top: 5px;">

                    <div class="img-map" style="height: 218px; width: 100%; background-image: url(https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $myhome_estate->get_position()['lat']; ?>,<?php echo $myhome_estate->get_position()['lng']; ?>&zoom=16&size=360x275&maptype=roadmap&markers=color:red%7Clabel:%7C<?php echo $myhome_estate->get_position()['lat']; ?>,<?php echo $myhome_estate->get_position()['lng']; ?>&key=<?php echo $map_key; ?>);"></div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col col-right" style="width: 56.5%; height: 443px;">
                <?php if( isset( $gallery_images[0]['image'] ) AND isset( $gallery_images[1]['image'] ) ): ?>
                <div class="row">
                    <div style="height: 150px;-webkit-print-color-adjust: exact">
                        <div id="image1"  style="background-image: url(<?php echo  $gallery_images[0]['image'] ?>);"></div>
                        <div id="image2" class="pull-right"  style="background-image: url(<?php echo  $gallery_images[1]['image'] ?>);"></div>
                    </div>
                </div>
                <?php endif; ?>
                <?php if( isset( $gallery_images[2]['image'] ) AND isset( $gallery_images[3]['image'] ) ): ?>
                <div class="row">
                    <div style="height: 150px;-webkit-print-color-adjust: exact">
                        <div id="image3" class="pull-left" style="background-image: url(<?php echo  $gallery_images[2]['image'] ?>);"></div>
                        <div id="image4"  style="background-image: url(<?php echo $gallery_images[3]['image'] ?>);"></div>
                    </div>
                </div>
                <?php endif; ?>
                <?php if( isset( $gallery_images[4]['image'] ) AND isset( $gallery_images[5]['image'] ) ): ?>
                <div class="row">
                    <div style="height: 140px;-webkit-print-color-adjust: exact">
                        <div id="image5" class="pull-left" style="background-image: url(<?php echo  $gallery_images[4]['image'] ?>);"></div>
                        <div id="image6"  style="background-image: url(<?php echo  $gallery_images[5]['image'] ?>);"></div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row" id="footer">
        <div  style="border: 1px solid #9d5919; border-radius: 25px 0 0 25px;">
            <table class="table">
                <tr>
                    <td width="15%">
                        <img src="<?php echo $this->plugin_url; ?>assets/img/Trusty-Group-HKJP.png" width="200">
                    </td width="20%">
                    <td id="agent-info">
                        <a href="<?php echo $myhome_agent->get_link(); ?>">
                            <div class="emp-photo-abs">
                                <img src="<?php echo $myhome_agent->get_image_url() ;?>">
                            </div>
                        </a>
                        <div style="width: 100%;">
                            <ul style="line-height: 1.3">
                                <li style="display: flex; align-items: center; justify-content:space-between;">

                                    <div class="agent-name">
                                        <div class="agent-chinese-name">
                                            <?php echo $myhome_agent->get_agentChineseName(); ?>
                                            <span class="agent-jobtitle"><?php echo $myhome_agent->get_jobTitle(); ?></span>
                                        </div>
                                        <div class="agent-english-name">
                                            <?php echo $myhome_agent->get_agentFullName(); ?>
                                        </div>
                                        <div class="agent-email">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <?php echo $myhome_agent->get_email(); ?>
                                        </div>
                                    </div>
                                    <div class="agent-number" style="margin-right: 10px;">
                                        <span>
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <?php echo $myhome_agent->get_phone(); ?>
                                        </span>
                                    </div>

                                </li>
                            </ul>
                        </div>
                    </td>
                    <td width="40%" id="cities">
                        <div class="agent-address" style="color: #5b442f; width: 100%">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <?php echo $myhome_agent->get_agentAddress(); ?>
                            <?php if( $myhome_agent->has_agentTelephone() ): ?>
                            <div>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <?php echo $myhome_agent->get_agentTelephone() ?>
                            </div>
                            <?php  endif;?>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</body>
</html>
