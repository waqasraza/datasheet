<?php

/**
 * Created by PhpStorm.
 * User: pak
 * Date: 9/12/2019
 * Time: 7:09 PM
 */
namespace Inc\Pages;

use Inc\Api\SettingsApi;
use Inc\Base\BaseController;
use Inc\Api\AdminCallbacks;

class Admin extends BaseController
{
    public $settings;
    public $callbacks;
    protected $pages;
    protected $subPages;

    public function __construct()
    {
        parent::__construct();

        $this->settings = new SettingsApi();
        $this->callbacks = new AdminCallbacks();

        $this->pages = array(
          array(
              'page_title'  => 'Estate Tool',
              'menu_title'  => 'Estate',
              'capability'  => 'manage_options',
              'menu_slug'   => 'estate_tool',
              'callback'    => array( $this->callbacks, 'toolEstate')
          )
        );
    }
    public function register()
    {
        $this->settings->addPages( $this->pages )->register();
    }




}