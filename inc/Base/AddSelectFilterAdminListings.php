<?php


namespace Inc\Base;


class AddSelectFilterAdminListings
{
    public function register()
    {
        add_action('restrict_manage_posts', array( $this, 'restrict_manage_posts' ), 10, 2);
    }

    public function restrict_manage_posts( $post_type, $which )
    {
        // Apply this only on a specific post type
        if ('estate' !== $post_type)  return;

        // A list of taxonomy slugs to filter by
        $taxonomies = array( 'region', 'city' );

        foreach ( $taxonomies as $taxonomy_slug ) {

            // Retrieve taxonomy data
            $taxonomy_obj = get_taxonomy( $taxonomy_slug );
            $taxonomy_name = $taxonomy_obj->labels->name;

            # myhome parent attribute region id
            $myhome_attribute_id = 10;

            if( isset( $_GET['region'] ) AND $_GET['region'] !== '' AND isset( $_GET['city'] ) AND $_GET['city'] !== '' AND  $taxonomy_slug == 'city'  ) {

                $region_term = get_term_by('slug', $_GET['region'], 'region');

                $args = array(
                    'taxonomy' => $taxonomy_slug,
                    'hide_empty' => false,
                    'hierarchical' => false,
                    'parent' => 0,
                     'meta_query' => array(
                        array(
                            'key' => 'term_parent_'.$myhome_attribute_id,
                            'value' => $region_term->term_id
                        ),
                     ),
                );
                // Retrieve taxonomy terms
                $terms = get_terms( $args );

            } else {
                // Retrieve taxonomy terms
                $terms = get_terms( $taxonomy_slug );

            }

            // Display filter HTML
            echo "<select name='{$taxonomy_slug}' id='list-{$taxonomy_slug}' class='postform' " . ( ( $taxonomy_slug == 'region' )  ? ' onChange=changeRegionSelectBox(this)' : '' ) . ">";

            if( !isset( $_GET['region'] ) OR $_GET['region'] == '' AND  !isset( $_GET['city'] ) OR $_GET['city'] == '' AND $taxonomy_slug == 'city'  ) {
                echo '<option value="">' . sprintf(esc_html__('Show All %s', 'text_domain'), $taxonomy_name) . '</option>';
            }

            foreach ( $terms as $term ) {
                printf(
                    '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
                    $term->slug,
                    ( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
                    $term->name,
                    $term->count
                );
            }
            echo '</select>';
        }
    }
}