<?php
/**
 * Created by PhpStorm.
 * User: pak
 * Date: 8/29/2019
 * Time: 3:18 PM
 */

namespace Inc\Base;


class Deactivate
{
    public static function deactivate()
    {
        flush_rewrite_rules();
    }

}