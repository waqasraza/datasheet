<?php
/**
 * Created by PhpStorm.
 * User: pak
 * Date: 8/29/2019
 * Time: 3:07 PM
 */

namespace Inc\Base;


class Activate
{
    public static function activate()
    {
        flush_rewrite_rules();
    }
}