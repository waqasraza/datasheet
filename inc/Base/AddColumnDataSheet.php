<?php
/**
 * Created by PhpStorm.
 * User: pak
 * Date: 8/29/2019
 * Time: 2:37 PM
 */

namespace Inc\Base;

use \Inc\Base\BaseController;

class AddColumnDataSheet extends BaseController
{

    public function register()
    {
        add_filter( "manage_estate_posts_columns", array( $this, 'columns_head' ) );
        add_action( "manage_estate_posts_custom_column", array( $this, 'columns_content' ), 10, 2);
    }
    public function columns_head( $columns ) {

        $columns_modified = array();
        foreach( $columns as $key => $title )
        {
            if( $key == 'taxonomy-property-type-new')
            {
                $columns_modified['data_sheet'] = 'Data Sheet';
            }
            else
            {
                $columns_modified[ $key ] = $title;
            }
        }
        return array_slice( $columns_modified, 0, 7 );

    }
    public function columns_content($column_name, $post_ID) {
        if ($column_name == 'data_sheet') {

//            if( get_post_status( $post_ID) == 'publish' ) {
                echo '<img src="' . $this->plugin_url . 'assets/img/Ajax-Preloader.gif"  class="ajax-preloader"/>';
                echo '<a href="javascript: void(0);" data-id="' . $post_ID . '" onClick="createPdf(this)" class="btn-datasheet hide">立即出盤紙!</a>';
//            }
        }
    }

}