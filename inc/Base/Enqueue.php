<?php
/**
 * Created by PhpStorm.
 * User: pak
 * Date: 8/30/2019
 * Time: 7:40 PM
 */

namespace Inc\Base;

use Inc\Base\BaseController;

class Enqueue extends BaseController
{
    public function register()
    {
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue') );
    }
    public function enqueue()
    {
        wp_enqueue_style('datasheet-css', $this->plugin_url.'assets/css/data_sheet.css');
        wp_enqueue_script('datasheet-js', $this->plugin_url.'assets/js/datasheet.js', array('jquery'));

        wp_localize_script( 'datasheet-js', 'datasheet', array(
            'ajax_url' => admin_url('admin-ajax.php')
        ) );
    }

}