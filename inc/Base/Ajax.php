<?php
/**
 * Created by PhpStorm.
 * User: pak
 * Date: 8/31/2019
 * Time: 2:31 PM
 */

namespace Inc\Base;

use Inc\Base\BaseController;

class Ajax extends BaseController
{
    public function register()
    {
        add_action('wp_ajax_createPdf', array( $this, 'createPdf' ) );
        add_action( 'wp_ajax_addKeyWordIntoListings', array( $this, 'addKeyWordIntoListings') );

        add_action('wp_ajax_changeRegionSelectBox', array( $this, 'changeRegionSelectBox' ) );
        add_action( 'wp_ajax_changeRegionSelectBox', array( $this, 'changeRegionSelectBox') );
    }
    public function createPdf()
    {
        $estate_id =  isset( $_POST['estate_id'] ) ? (int) $_POST['estate_id'] : 0;
        $file = $this->plugin_path . 'templates/datasheet.php';

        if( $estate_id > 0 ){}

        if( ! file_exists( $file ) ){}

        ob_start();
        require_once $file;
        $output = ob_get_clean();
        echo json_encode($output);
        exit;
    }
    public function addKeyWordIntoListings()
    {
        # Get all estates
        $args = array(
            'post_type'     => 'blog',
            'post_status'   => 'publish',
            'posts_per_page'=> -1,
            'meta_query'    => array(
                'relation' => 'AND',
                array(
                    'key'       => '_yoast_wpseo_focuskw',
                    'compare'   => 'NOT EXISTS'
                )
            )
        );

        $WP_Query = new \WP_Query( $args );

        if( count( $WP_Query->get_posts() ) )
        {
            foreach( $WP_Query->get_posts() as $post )
            {
                add_post_meta( $post->ID, '_yoast_wpseo_focuskw', '日本樓' );
            }

            echo 'Done!!';die;
        }

    }
    public function changeRegionSelectBox()
    {
        $region_slug = isset( $_POST['slug'] ) ? $_POST['slug'] : '';

        $term = get_term_by('slug', $region_slug, 'region');

        # myhome parent attribute region id
        $myhome_attribute_id = 10;

        $args = array(
            'taxonomy' => 'city',
            'hide_empty' => false,
            'hierarchical' => false,
            'parent' => 0,
        );

        if( ! empty( $region_slug )  ) {

            $args['meta_query'] = [
                [
                    'key' => 'term_parent_'.$myhome_attribute_id,
                    'value' => $term->term_id
                ],
            ];
        }

        $html = '';
        $terms = get_terms( $args );
        if ( !empty( $terms ) && !is_wp_error( $terms ) ) {
            foreach ( $terms as $term ) {
                if( $term->count ) $html .= '<option value="'. $term->slug .'" %2$s>'. $term->name .' ('. $term->count .')</option>';
            }
        }

       echo  wp_json_encode( array( 'html' => $html ));wp_die();
    }

}