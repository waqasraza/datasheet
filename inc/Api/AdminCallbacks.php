<?php

/**
 * Created by PhpStorm.
 * User: pak
 * Date: 9/13/2019
 * Time: 7:14 PM
 */
namespace Inc\Api;

use Inc\Base\BaseController;

class AdminCallbacks extends BaseController
{
    public function toolEstate()
    {
        require_once "$this->plugin_path/templates/estate_settings.php";
    }

}